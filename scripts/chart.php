<?php

/**
 * @file
 * Displays a chart and filter form with tags and there times.
 */

// Include Guzzle, bootstrap, csvparser.
require_once __DIR__ . '/../vendor/autoload.php';

use Twig\Extension\DebugExtension;
use TMetric\Access\MySqliConnect;
use TMetric\Classes\ParseDurationTime;
use TMetric\Classes\TimeMetric;

// Include twig classes.
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

// For creating twig classes and functions.
use Twig\TwigFunction;

// Load twig.
$loader = new FilesystemLoader('./themes');
// For debugging.
$twig = new Environment($loader, ['debug' => TRUE]);
$twig->addExtension(new DebugExtension());

// Setup global variables.
$twig->addGlobal('post', $_POST);
$twig->addGlobal('get', $_GET);
$twig->addGlobal('files', $_FILES);
$twig->addGlobal('cookie', $_COOKIE);

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();
$timeMetric = new TimeMetric();
$parseDurationTime = new ParseDurationTime();

// Access validation.
$validate = new \TMetric\Access\Validation();
$validate->validate();

// Include menu.
require_once __DIR__ . '/menu.php';

// Add function to twig.
$twig->addFunction(new TwigFunction('getDurations', 'get_duration_minutes'));

/**
 * Function for get duration task times.
 *
 * Get utc and simple time from inhours format.
 *
 * @param array $taskMinutes
 *   Time task in minutes.
 *
 * @return array
 *   taskDurations, taskDurationsString.
 */
function get_duration_minutes(array $taskMinutes) {

  // отримуєм з масиву що містить хв, масив з год і хв по таскам.
  foreach ($taskMinutes as $index => $taskTime) {
    if ($taskTime >= 60) {
      if (mb_strlen((int) ($taskTime / 60)) < 2) {
        $h = '0' . (int) ($taskTime / 60);
      }
      else {
        $h = (int) ($taskTime / 60);
      }

      $explmm = explode('.', round($taskTime / 60, 2));

      if (isset($explmm[1])) {
        $mm = '0.' . $explmm[1];
      }
      else {
        $mm = 0;
      }

      if ($mm === 0) {
        $m = '00';
      }
      else {
        if (mb_strlen((int) ($mm * 60)) < 2) {
          $m = '0' . (int) ($mm * 60);
        }
        else {
          $m = (int) ($mm * 60);
        }
      }
    }
    else {
      $h = '00';

      if (mb_strlen((int) $taskTime) < 2 && $taskTime !== 0) {
        $m = '0' . $taskTime;
      }
      elseif ($taskTime === 0) {
        $m = '00';
      }
      else {
        $m = $taskTime;
      }
    }

    $taskDurations[] = $h . ':' . $m . '';
    $taskDurationsString[] = $h . 'h:' . $m . 'm';
  }

  return [
    'taskDurations' => $taskDurations,
    'taskDurationsString' => $taskDurationsString,
  ];
}

// Set days period for HTML filters form, if unset.
if (!isset($_POST['filter'])) {
  $firstDayOfMonth = date('Y-m-01');
  $lastDayOfMonth = date('Y-m-t');
  $_COOKIE['select-user'] = '';
}
// Set days period for HTML filters form, if set.
elseif (isset($_POST['filter'])) {
  $firstDayOfMonth = $_POST['start-date'];
  $lastDayOfMonth = $_POST['end-date'];
  $_COOKIE['select-user'] = htmlspecialchars($_POST['select-user']);
}
$getTimeEntriesReport = $timeMetric->getTimeEntriesDetailedReport($firstDayOfMonth, $lastDayOfMonth);
$_COOKIE['firstDayOfMonth'] = $firstDayOfMonth;
$_COOKIE['lastDayOfMonth'] = $lastDayOfMonth;


/**
 * Function print filter form for chart.
 *
 * @var array $userMembers
 *   Actual users and projects.
 * @var int   $memberId
 *   Member id.
 * @var array $memberName
 *   Member name.
 * @var array $members
 *   Members names and IDs.
 * @var array $_COOKIE ['firstDayOfMonth']
 *   First day of month.
 * @var array $_COOKIE ['lastDayOfMonth']
 *   Last day of month.
 *
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
$twig->addFunction(new TwigFunction('tmetric_filter', 'show_tmetric_filter'));

/**
 * Function shows filter block for Chart.
 */
function show_tmetric_filter() {
  ?>
    <div class="container pt-6">
        <div class="row justify-content-center align-items-center">
            <form name=""
                  id=""
                  class="user-filter m-1 badge badge-dark border border-info"
                  method="post">
                <div class="filter-options">

                    <div class='select-users'>
                        <u>SELECT &nbsp USER</u>
                        <input list='users'
                               id='select-user'
                               name='select-user'
                               class='select-user'
                               value="<?php print $_COOKIE['select-user']; ?>"
                               type='text'
                               autocomplete='off'>

                        <datalist id='users'>
                          <?php
                          $timeMetric = new TimeMetric();
                          $userMembers = $timeMetric->accountMembers();
                          $m = 1;

                          foreach ($userMembers as $key => $member) {
                            $memberId = array_key_first($member);
                            $memberName = $member[$memberId];
                            $members[] = $memberName;
                            print "<option name='member{$m}' id='member{$m}' class='member{$m}'> {$memberName}";
                            ++$m;
                          }
                          print '</option>';
                          ?>
                        </datalist>

                    </div>

                    <div class="date-filter">
                        <label for="date"
                               class="">Interval:
                        </label>
                        <input type="date"
                               id="start-date"
                               name="start-date"
                               class="start-date"
                               value="<?php print $_COOKIE['firstDayOfMonth']; ?>"/>
                        <input type="date"
                               id="end-date"
                               name="end-date"
                               class="end-date"
                               value="<?php print $_COOKIE['lastDayOfMonth']; ?>"/>
                        <div class="row justify-content-center">
                            <button type="submit"
                                    name="filter"
                                    class="btn-accept badge-pill w-25">Accept
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
  <?php
}

// On start $_POST['select-user'] is not defined.
// Doing it empty.
if (!isset($_POST['select-user'])) {
  $_POST['select-user'] = '';
}

/**
 * Function parses csv file and get tags, projects names, tasks durations.
 *
 * @return array
 *   'taskDurationsVariables' => $taskDurationsVariables,
 *   'projectTagsArray' => $projectTagsArray,
 *   'projectNamesArray' => $projectNamesArray
 *
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function get_task_info() {
  $timeMetric = new TimeMetric();
  $parseDurationTime = new ParseDurationTime();
  if (!isset($_POST['filter'])) {
    $firstDayOfMonth = date('Y-m-01');
    $lastDayOfMonth = date('Y-m-t');
  }
  elseif (isset($_POST['filter'])) {
    $firstDayOfMonth = $_POST['start-date'];
    $lastDayOfMonth = $_POST['end-date'];
  }
  $getTimeEntriesReport = $timeMetric->getTimeEntriesDetailedReport($firstDayOfMonth, $lastDayOfMonth);

  // Using filter with selected user.
  if (!empty($_POST['select-user']) && !empty($_COOKIE['select-user'])) {
    $selectedUSer = htmlspecialchars($_POST['select-user']);

    foreach ($getTimeEntriesReport as $item => $task) {
      $taskUser = $task['user'];
      if ($taskUser === $selectedUSer) {
        // Set tag if its exist.
        if (isset($task['tags'][0])) {
          $tag = $task['tags'][0];
        }
        // Set tag if its absent.
        else {
          $tag = 'UNDEFINED!!!';
        }
        // Set project name if its exist.
        if (isset($task['project'])) {
          $projectNamesArray[] = $task['project'];
        }
        // Set project name if its absent.
        else {
          $task['project'] = 'UNDEFINED!!!';
          $projectNamesArray[] = 'UNDEFINED!!!';
        }
        $projectTagsArray[] = $tag;
        // Set duration in minutes for each task.
        $taskMinutes[] = round(($task['duration'] / 1000 / 60), 2);
        // Rebuild duration in minutes.
        $taskDurationsVariables = $parseDurationTime->getDurationWithMinutes($taskMinutes);
      }
    }
  }
  // Default view for Chart - if no chosen filter.
  else {

    foreach ($getTimeEntriesReport as $item => $task) {
      // Set tag if its exist.
      if (isset($task['tags'][0])) {
        $tag = $task['tags'][0];
      }
      // Set tag if its absent.
      else {
        $tag = 'UNDEFINED!!!';
      }
      // Set project name if its exist.
      if (isset($task['project'])) {
        $projectNamesArray[] = $task['project'];
      }
      // Set project name if its absent.
      else {
        $task['project'] = 'UNDEFINED!!!';
        $projectNamesArray[] = 'UNDEFINED!!!';
      }
      $projectTagsArray[] = $tag;
      // Set duration in minutes for each task.
      $taskMinutes[] = round(($task['duration'] / 1000 / 60), 2);
      // Rebuild duration in minutes.
      $taskDurationsVariables = $parseDurationTime->getDurationWithMinutes($taskMinutes);
    }
  }
  // If no data in this date - the array is empty.
  // Exception for return.
  try {
    if (empty($taskDurationsVariables) and empty($projectTagsArray) and empty($projectNamesArray)) {
      /*
       * Throw new Exception(
       * "<p>
       * <b class='alert'>Array is empty!!!</b>
       * </p>");.
       */
    }
    else {
      return [
        'taskDurationsVariables' => $taskDurationsVariables,
        'projectTagsArray' => $projectTagsArray,
        'projectNamesArray' => $projectNamesArray,
      ];
    }
  }
  catch (Exception $ex) {
    // Вbводим сообщение об исключении.
    echo $ex->getMessage();
  } finally {
  }

}

// Get tasks tag duration.
$getTaskInfo = get_task_info();
if (empty($getTaskInfo)) {

}
else {
  $taskDurations = $getTaskInfo['taskDurationsVariables']['taskDurations'];
  $musDurations = $parseDurationTime->getDurationWithLabelTag($getTaskInfo['projectTagsArray'], $taskDurations);
  // Make tags array with there duration.
  $tagsDuration = $parseDurationTime->getSummaryDurationTime($musDurations);

  // Make tag array with total durations.
  foreach ($tagsDuration as $tag => $duration) {
    foreach ($duration as $key => $time) {
      $s = '00';
      $explTime = explode(':', $time);

      if ($key === 0) {
        $h = $explTime[0];
        $m = $explTime[1];
      }
      elseif ($key > 0) {
        $m += $explTime[1];

        if ($m < 10) {
          $m = '0' . $m;
        }

        if ($m > 60) {
          $h = $h + 1;
          $m = $m % 60;
        }
        $h += $explTime[0];
      }
    }

    if (mb_strlen($h) < 2) {
      $h = '0' . $h;
    }
    elseif (mb_strlen($m) < 2) {
      $m = '0' . $m;
    }

    $tagDurationSums = (string) ($h . ':' . $m . ':' . $s);
    $tagDurationSum = (string) ($h . ':' . $m);
    $tagDurationStringSum = (string) ($h . 'h:' . $m . 'm');
    $generalTagsDuration[$tag] = $duration;

    if (empty($totalTagsDuration)) {
      $totalTagsDuration = [$tag => $tagDurationSum];
    }
    else {
      $totalTagsDuration += [$tag => $tagDurationSum];
    }
  }
  // Create persent status for js chart.
  $projectNamesArray = $getTaskInfo['projectNamesArray'];
  // Sort() add new 'projectNamesArray' in $getTaskInfo,
  // instead use $projectNamesArray.
  sort($getTaskInfo['projectNamesArray']);
  $tagNames = array_unique($getTaskInfo['projectTagsArray']);
  $countingTags = array_count_values($getTaskInfo['projectTagsArray']);
  $countTagsValues = $totalTagsDuration[$tag];
  $ew = explode(':', $countTagsValues);

  // Отримуємо суму часу усіх тегів.
  foreach ($totalTagsDuration as $tag => $tagTime) {
    $explTime = explode(':', $tagTime);
    $h = $explTime[0] * 60;
    $m = $explTime[1];
    // По тегам окремо.
    $sumTagsTimes[$tag] = $h + $m;
    // По всім тегав взагалом.
    if (empty($sumAllTagsTimes)) {
      $sumAllTagsTimes = $h + $m;
    }
    else {
      $sumAllTagsTimes += ($h + $m);
    }
  }

  arsort($sumTagsTimes);
  // Переводимо час тегів в процентному відношенні.
  foreach ($sumTagsTimes as $tag => $duration) {
    $prc = 100;
    $one = $prc / $sumAllTagsTimes;

    if (($one * $duration) < 1) {
      $projectsStatus[] = '< 1%';
      $projectsPercents[] = (round($one * $duration, 2));
    }
    else {
      $projectsStatus[] = (round($one * $duration, 2)) . '%';
      $projectsPercents[] = (round($one * $duration, 2));
    }
  }

  // Add persent to project label for js chart.
  $tagsCount = count($generalTagsDuration);

  for ($i = 0; $i < $tagsCount; ++$i) {
    if (empty($tagsLabels)) {
      $tagsLabels[] = $projectsStatus[$i] . '_' . current($tagNames) . ' (' . current($totalTagsDuration) . ')';
    }
    else {
      $tagsLabels[] = $projectsStatus[$i] . '_' . next($tagNames) . ' (' . next($totalTagsDuration) . ')';
    }
  }

}

/*
 * Render needed variables which used in twig template.
 * If you need to load only log_out templete without using include().
 * echo  $twig->render( 'log_out.html.twig',[
 * If you need to load one templete which contains other templates.
 * echo  $twig->render( 'chart_blocks.html.twig',[
 */
echo $twig->render('chart.html.twig', [
  'getTimeEntriesReport' => (new TimeMetric)->getTimeEntriesDetailedReport($firstDayOfMonth, $lastDayOfMonth),
  'selectedUSer' => htmlspecialchars($_POST['select-user']),
  'cookieSelectUser' => $_COOKIE['select-user'],
    // log_out.
  'activeUser' => $GLOBALS['activeUser'],
]);

?>
<script type='text/javascript'>

    /**
     * Function generate random colors.
     *
     * @return array colors
     */
    function generateRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;

    }

    /**
     * Function create chart with task tags and percents of time
     *
     * @var php_tagsNamesArray
     *     Contains tags names array.
     * @var countingTags
     *     Contains tags percents array.
     * @var tagsCount
     *     Contains tags count.
     * @var tagsNamesArray
     *     Splited php_tagsNamesArray.
     * @var countColors
     *     = tagsCount.
     * @var masColors
     *     GenerateRandomColor() for count tags.
     * @var colorsArray
     *     Splited masColors.
     * @var config
     *     All options for Chart.
     */
    window.onload = function () {
        let php_tagsNamesArray = [<?php print '"' . implode('","', $tagsLabels)
          . '"'; ?>];
        let tagsNamesArray = php_tagsNamesArray.toString().split(",");
        let countingTags = [<?php print '"' . implode('","', $projectsPercents)
          . '"'; ?>];
        let tagsCount = <?php print json_encode($tagsCount); ?>;
        let countColors = tagsCount;
        let masColors = [];
        for (var cc = 0; cc < countColors; cc++) {
            masColors[cc] = generateRandomColor();
        }
        let colorsArray = masColors.toString().split(",");
        new Chart(document.getElementById("chartjs-doughnut"),
            {
                "type": 'doughnut',
                "data": {
                    "datasets": [{
                        "data": countingTags,
                        "backgroundColor": colorsArray,
                        "borderColor": generateRandomColor(),
                        "borderWidth": 2,
                        "width": 10,
                        "label": 'Dataset 1'
                    }],
                    "labels": tagsNamesArray,
                },
                "options": {
                    "responsive": true,
                    "legend": {
                        "position": 'right',
                        "boxWidth": 10,
                        "labels": {
                            "fontColor": 'black',
                            "fontSize": 14,
                            "fontStyle": 'bold',
                            "boxWidth": 10,
                        },
                        "animation": {
                            "animateScale": true,
                            "animateRotate": true
                        }
                    },
                }
            });
    }
</script>

<?php

/**
 * @file
 * Displays a form for enter/registration.
 *
 * Available variables:
 * - $access_Sql: Contains SQL access for users.
 * - $htmlUserEmail: Contains current user mail.
 * - $mysqlEmail: Contains mysql mail.
 * - $count_users: Contains count of users.
 * - $checkAccess: Contains access from sql db.
 * - $changeForEmail: For whom is changing.
 * - $changePassw: What pass will be changed from msql.
 */

// Inclide Guzzle, bootstrap, csvparser.
require_once __DIR__ . '/../vendor/autoload.php';
require_once 'menu.php';

// Include twig.
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

// Include mysqli.
use TMetric\Access\MySqliConnect;

// Load twig themes path and enviroment.
$loader = new FilesystemLoader('themes');
$twig = new Environment($loader);

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

// Set global variable for twig like a $_POST.
$twig->addGlobal('post', $_POST);

// Unset cookie for clear enter.
unset($_COOKIE['changePassword']);
// If headers already sent out.
if (headers_sent()) {
}
else {
  setcookie('changePassword', NULL, -1, '/');
}

// Access validation.
$validate = new \TMetric\Access\Validation();
$validate->validate();
$access_Sql = $msql->query('SELECT * FROM `login` ')->fetch_all();
$count_users = count($access_Sql);

// Reset mysqli id.
$mySqliConnect->rebuildId('login');
$mySqliConnect->resetId('login');

/*
 * Set header type for html.
 * header('Content-type: text/html; charset=utf-8; Content-Length: 0');
 */

// Delete users.
for ($i = 1; $i <= $count_users; ++$i) {
  if (isset($_POST['delete_' . $i])) {
    $msql->query("DELETE FROM `login` WHERE `id`='{$i}'");
    $mySqliConnect->rebuildId('login');
    $mySqliConnect->resetId('login');
  }
}

// Change password.
for ($i = 1; $i <= $count_users; ++$i) {
  if (isset($_POST['changePswrd_' . $i])) {
    $changePasswSQL = $msql->query("SELECT `email`, `password` FROM `login` where `id`= '{$i}'")
      ->fetch_array();
    $changeForEmail = $changePasswSQL['email'];
    $changePassw = $changePasswSQL['password'];
    $_COOKIE['changeFor'] = $changeForEmail;
    $_COOKIE['changePassword'] = $changePassw;
    setcookie('changeFor', $changeForEmail);
    setcookie('changePassword', $changePassw);
    header('location: change_password');
  }
}

if (isset($_POST['save-accecc-btn'])) {
  // Getting html users array.
  foreach ($_POST as $item => $value) {
    $user = preg_match('/user/', $item);
    if ($user == 1) {
      $htmlUsers[] = $value;
    }
  }
  // Getting mysqli users array.
  $access_Sql = $msql->query('SELECT `email`, `access` FROM `login` ')
    ->fetch_all();
  $n = 0;

  foreach ($access_Sql as $emailUser => $email) {
    if ($n == 0) {
      $mysqlEmail = [$emailUser => $email[0]];
      ++$n;
    }
    else {
      $mysqlEmail += [$emailUser => $email[0]];
    }
  }
  // Set mysqli users access.
  $count_users = count($access_Sql);

  for ($a = 1; $a <= $count_users; ++$a) {
    $access = 'access_';
    $checkAccess = array_key_exists($access . $a, $_POST);
    $userEmail = $_POST['user_' . $a];

    if ($checkAccess === TRUE) {
      $mySqliConnect->updateData('login', 'access=1', 'email=' . $userEmail);
    }
    elseif (($userEmail === $_COOKIE['user_email']) && $checkAccess === FALSE) {
      $mySqliConnect->updateData('login', 'access=0', 'email=' . $userEmail);
      clearstatcache();
      unset($_COOKIE['user_email'], $_COOKIE['user_password'], $_COOKIE['message']);

      setcookie('user_email', NULL, -1, '/');
      setcookie('user_password', NULL, -1, '/');
      setcookie('message', NULL, -1, '/');
      header('location: login');

      exit;
    }
    elseif ($checkAccess === FALSE) {
      $mySqliConnect->updateData('login', 'access=0', 'email=' . $userEmail);
    }
  }
  // Updating users email if they was changed in HTML input.
  foreach ($htmlUsers as $key => $htmlUserEmail) {
    if ($htmlUserEmail !== $mysqlEmail[$key]) {
      $mySqliConnect->updateData('login', 'email=' . $htmlUserEmail, 'email=' . $mysqlEmail[$key]);
    }
  }
}
// Array for twig template.
$access_Sql = $msql->query('SELECT * FROM `login` ')->fetch_all();

/*
 * for single self block intagration:
 * $temple = $twig->load('access.html.twig');
 * and for multiple integration blocks:
 */
$temple = $twig->load('access.html.twig');
echo $temple->render([
  'access_Sql' => $access_Sql,
  // Form log_out.
  'activeUser' => $passActiveUser,
]);

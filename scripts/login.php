<?php

/**
 * @file
 * Display entering and registration form.
 */

// Inclide Guzzle, bootstrap, csvparser.
require_once __DIR__ . '/../vendor/autoload.php';

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use TMetric\Access\MySqliConnect;

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

header('Content-type: text/html; charset=utf-8');

$loader = new FilesystemLoader('./themes');
$twig = new Environment($loader);
$twig->addGlobal('post', $_GET);
$twig->addGlobal('get', $_GET);

session_start();

// Check for isset user in mysql.
$message = '';
$mySqliConnect->resetId('login');
$checkUserArray = $msql->query('SELECT `email`, `password` ,`access` FROM `login` ');
$checkUser = $checkUserArray->fetch_all();

if (empty($checkUser)) {
  $message = '<h5>Зарегистрируйесь, вы еще не зарегестрированы!</h5> ';
}
elseif (isset($_COOKIE['message'])) {
  $message = $_COOKIE['message'];
}
else {
  $message = 'Введите ваш email и пароль:';
}

// Variables for changing classes.
$active = 'active';
$passive = 'passive';
$nameEnter = 'enter';
$enterVal = 'Войти';

// Registration add or update user fields.
$res = $msql->query('SELECT id FROM `login`');
$count = mysqli_num_rows($res);

if (isset($_POST['register'])) {
  $eMail = $_POST['access_email'];
  $pass_crypt = crypt($_POST['access_password'], 'tmetric');

  if (empty($eMail) or empty($_POST['access_password']) or (empty($eMail) and empty($_POST['access_password']))) {
    $message = "Не введенны все данные!";
  }
  elseif ($count > 0) {
    $mySqliConnect->resetId('login');
    $msql->query("INSERT INTO `login` (`email`, `password`) VALUES  ('{$eMail}', '{$pass_crypt}')");
    $message = ("$eMail добавлен!");
  }
  elseif ($count === 0) {
    $register = $msql->query("INSERT INTO `login` (`email`, `password`) VALUES  ('{$eMail}', '{$pass_crypt}')");
    $message = ("$eMail добавлені!");
  }
}

// Checking users data and enter on main page.
if (isset($_POST['enter'])) {
  $input_email = $_POST['access_email'];
  $input_password_crypt = crypt($_POST['access_password'], 'tmetric');

  // Set the cookie.
  setcookie('user_email', $input_email);
  setcookie('user_password', $input_password_crypt);

  // If password = mysql password.
  foreach ($checkUser as $key => $value) {
    $mysql_email = $value['0'];
    $mysql_password = $value['1'];
    $mysql_access = $value['2'];

    if ($mysql_email == $input_email && $mysql_password == $input_password_crypt && $mysql_access == 1) {
      header('location: chart');
    }
    elseif ($mysql_email == $input_email && $mysql_password ==
        $input_password_crypt && $mysql_access == 0) {
      $message = 'У вас нет доступа!';
    }
    elseif ($mysql_email == $input_email && $mysql_password !==
          $input_password_crypt && $mysql_access == 0) {
      $message = 'Неверный пароль и нет доступа!';
    }
    elseif ($mysql_email == $input_email && $mysql_password !== $input_password_crypt && $mysql_access == 1) {
      $message = 'Пароль не совпадает!';
    }
    elseif (empty($input_email) and empty($_POST['access_password'])) {
      $message = 'Заполните все поля!';
    }
    elseif (empty($input_email)) {
      $message = 'Вы не ввели еmail!';
    }
    elseif (empty($_POST['access_password'])) {
      $message = 'Вы не ввели пароль!';
    }
    else {
      $message = 'Введенны неверные данные!';
    }
  }
}

// Not necessary block.
if (isset($_POST['start-enter'])) {
  $active = 'active';
  $passive = 'passive';

}
// If use register.
elseif (isset($_POST['start-register'])) {
  // When use register its change classes for tips.
  $active = 'passive';
  $passive = 'active';
  // It changes button ENTER for REGISTER.
  $nameEnter = 'register';
  $enterVal = 'Регистрация';
}

// Load start template with variables.
$temple = $twig->load('login.html.twig');
echo $temple->render([
  'message' => $message,
  'active' => $active,
  'passive' => $passive,
  'nameEnter' => $nameEnter,
  'enterVal' => $enterVal,
]);

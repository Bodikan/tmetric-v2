<?php

/**
 * @file
 * Display main Menu at the left side (reports,chart,access).
 */

//require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/../vendor/autoload.php';
/*
 * Set header type for html.
 * header('Content-type: text/html; charset=utf-8; Content-Length: 0');
 */

// Include mysqli.
use TMetric\Access\MySqliConnect;

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

// Checking who is active user.
$activeUserSQL = $msql->query('SELECT `email` FROM `login` ')->fetch_all();

foreach ($activeUserSQL as $user => $data) {
  if ($data[0] === $_COOKIE['user_email']) {
    $activeUser = $data[0];
    // Ror passing $activeUser to another file.
    $GLOBALS['activeUser'] = $data[0];
  }
}
// Ror passing $activeUser to another file.
$passActiveUser = $activeUser;

// Processing button for exit.
if (isset($_POST['logout-btn'])) {
  clearstatcache();
  unset($_COOKIE['user_email'], $_COOKIE['user_password'], $_COOKIE['message']);

  setcookie('user_email', NULL, -1, '/');
  setcookie('user_password', NULL, -1, '/');
  setcookie('message', NULL, -1, '/');
  header('location: login');

  exit;
}

// Create same arrays for array_map() which use
// $fileLocations with 3 variables.
// Can use $GLOBALS['scriptName'], $GLOBALS['scriptSelfDir'].
$scriptSelfDir = [
  '1' => dirname($_SERVER['PHP_SELF']),
  '2' => dirname($_SERVER['PHP_SELF']),
  '3' => dirname($_SERVER['PHP_SELF']),
];
 $scriptHttp = $_SERVER['HTTP_REFERER'];
$scriptName = [
  '1' => basename($_SERVER['SCRIPT_NAME']),
  '2' => basename($_SERVER['SCRIPT_NAME']),
  '3' => basename($_SERVER['SCRIPT_NAME']),
];

/**
 * Function watch current script path and sent it to Menu.php.
 *
 * @param string $file
 *   Param is empty.
 * @param string $scriptName
 *   Name of script self.
 * @param string $scriptSelfDir
 *   Path of script self.
 *
 * @return string
 *   $path.$file
 */
function change_script_path($file, $scriptName, $scriptSelfDir) {
  // Can use global $scriptName, $scriptSelfDir
  // or $GLOBALS['scriptName'], $GLOBALS['scriptSelfDir']
  // or &variables.
  if ($scriptName == 'change_password.php' and $scriptSelfDir == '/access') {
    $path = '../';
  }
  return $path . $file;
}

$fileLocations = [
  'tmetric' => 'tmetric',
  'chart' => 'chart',
  'admission' => 'admission',
];

// On this page path_root = /access, go to base path.
if ($scriptName[1] == 'change_password.php' and $scriptSelfDir[1] == '/access') {

  $fileLocations = array_map('change_script_path', $fileLocations, $scriptName,
    $scriptSelfDir);

  foreach ($fileLocations as $key => $item) {
    $key = basename($item, ".php");
    array_shift($fileLocations);
    $fileLocations[$key] = $item;
  }
}
// Using scripts for all the buttons.
if (isset($_POST['tmetric-btn'])) {

  header('Location: ' . $fileLocations['tmetric']);

}

if (isset($_POST['report-btn'])) {

  header('Location: ' . $fileLocations['chart']);
}

if (isset($_POST['accecc-btn'])) {

  header('Location: ' . $fileLocations['admission']);

}

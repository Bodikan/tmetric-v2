<?php

/**
 * @file
 * Display page with parsing file menu.
 */

// Inclide Guzzle, bootstrap, csvparser.
//require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/../vendor/autoload.php';
// Additional tools.
require_once __DIR__ . '/menu.php';

// Include classes namespaces.
use Twig\Extension\DebugExtension;
use TMetric\Classes\CSVImporter;
use TMetric\Classes\TimeMetric;
use TMetric\Classes\ProjectsTimes;

// Include twig namespaces.
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

// Connection to MySQLI.
use TMetric\Access\MySqliConnect;

$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

// Calling classes.
// Load and parsing csv file.
$csvImporter = new CSVImporter();
$timeMetric = new TimeMetric();
$projectTimes = new ProjectsTimes();

// Load twig themes path and environment.
$loader = new FilesystemLoader('./themes');
// For debugging.
$twig = new Environment($loader, ['debug' => TRUE]);
$twig->addExtension(new DebugExtension());
// Setup global variables.
$twig->addGlobal('post', $_POST);
$twig->addGlobal('get', $_GET);
$twig->addGlobal('files', $_FILES);

// Create log file.
ini_set('error_log', 'error_log');

// Access validation.
$validate = new \TMetric\Access\Validation();
$validate->validate();

// Include files.
$jira = 'jira.csv';
$clockify = 'clockify.csv';

if (isset($_POST['parseBtn']) && ($_FILES['parsingJira']['name'] === '' && $_FILES['parsingClockify']['name'] === '')) {

  // If file wasnt chosen.
  print " 
<div class='mx-auto' style='width: 400px;'>
		 	<span class='message-file bg-dark'>
		    You must chose the file at first!
		  </span>
 </div>";
}
// Set header type for html.
// If headers already sent out.
if (headers_sent()) {
}
else {
  header('Content-type: text/html; charset=utf-8; Content-Length: 0');
}

// после того как нажата кнопка парсинга,
// имя файла доступно из id тега загрузки файла.
if (isset($_FILES['parsingJira']) && $_FILES['parsingJira']['error'] === \UPLOAD_ERR_OK) {
  // Get details of the uploaded file.
  $newFileName = 'jira.csv';
  $fileTmpPath = $_FILES['parsingJira']['tmp_name'];
}
elseif (isset($_FILES['parsingClockify']) && $_FILES['parsingClockify']['error'] === \UPLOAD_ERR_OK) {
  $newFileName = 'clockify.csv';
  $fileTmpPath = $_FILES['parsingClockify']['tmp_name'];
}
else {
  // файл изначально не вибран по этому он пуст.
  $newFileName = '';
}

// Array for adding twig function.
$_SESSION['twigVariables'] = [];
// После проверки запроса POST мы проверяем, что загрузка файла прошла успешно.
if (isset($_FILES['parsingJira']) || isset($_FILES['parsingClockify'])) {

  // Folder in which the uploaded file will be moved.
  $uploadFileDir = 'csv/';
  $file_path = $uploadFileDir . $newFileName;
  $_SESSION['file_location'] = $file_path;

  if ($newFileName === $jira || $newFileName === $clockify) {
    move_uploaded_file($fileTmpPath, $file_path);
    // Parse file.
    $csvArray = $csvImporter->csvParseFile();
    $_SESSION['csvArray'] = $csvArray;
    $headersFile = $csvImporter->headersFile();
    /*
     * $csvGetData = new ProjectsTimes();
     * $csvParsedTimes = $csvGetData->timeEntries();
     */
    $userMembers = $timeMetric->accountMembers();
    $_SESSION['userMembers'] = $userMembers;
    $projectsArray = $timeMetric->projectInfo();
    $_SESSION['projectsArray'] = $projectsArray;

    $n = 0;
    $set = 1;
    $row = 0;
    $csvRow = $row + 1;
    $p = 1;

    // Entering project array with names and values.
    foreach ($projectsArray as $key => $project) {
      $projectName = $project['projectName'];
      $projectsNames[$csvRow][] = $projectName;
      ++$p;
      $_SESSION['projects'] = $projectsNames[1];
    }


    $m = 1;
    // Create select with tmetric members.
    foreach ($userMembers as $key => $member) {
      $memberId = array_key_first($member);
      $memberName = $member[$memberId];
      $members[] = $memberName;
      ++$m;
    }

    ++$set;
    // Array for twig which is empty on the start,
    // and filling when is parsing thr file.
    $_SESSION['twigVariables'] = [
      'csvParsedTimes' => (new ProjectsTimes)->timeEntries(),
      'userMembers' => (new TimeMetric)->accountMembers(),
      'projectsArray' => (new TimeMetric)->projectInfo(),
    ];
  }
  else {
    print "<div class='message mx-auto' style='width: 400px;'>
		        	<span class='message-file bg-dark'>
                There was some error moving the file to upload directory (./csv/).<br>
                Please make sure the upload directory is writable and file has correct name
		          </span>
           </div>";
  }
}
// IF start = everything is empty.
else {
}

// Sending data to TMetric for creating timeentries.
if (isset($_POST['save-times'])) {
  $csvArray = $_SESSION['csvArray'];
  $tasks = $_SESSION['fileTasksArray'];

  $number = 1;
  // Delete first element in POST array.
  $postArray = array_shift($_POST);

  // v1 составим масив с  выбранымии не выбраными елементами.
  for ($i = 0; $i < \count($csvArray) + 1; ++$i) {
    $project = $_POST["select-project{$number}"];
    $user = $_POST["select-user{$number}"];
    $select_project = htmlspecialchars($_POST["select-project{$number}"]);
    $select_user = htmlspecialchars($_POST["select-user{$number}"]);

    $chosenTasks[$i] = [
      'select-project' => $select_project,
      'select-user' => $select_user,
    ];
    ++$number;
  }
  // If project or users wasnt selected.
  if (empty($select_project)) {
    $emptyProjects = TRUE;
    $emptyUsers = FALSE;
    goto stop;

  }
  elseif (empty($select_user)) {
    $emptyUsers = TRUE;
    $emptyProjects = FALSE;
    goto stop;

    // If selected all projects and users.
  }
  elseif (!empty($select_project) and !empty($select_user)) {

    $emptyProjects = FALSE;
    $emptyUsers = FALSE;

    foreach ($chosenTasks as $item => $chose) {

      // Cheking projects names from array with selected projects names on page.
      foreach ($_SESSION['projectsArray'] as $key => $prj) {
        $projectName = $prj['projectName'];

        if ($projectName === $chose['select-project']) {
          $projectId = $prj['projectId'];
          $chosenTasks[$item] += ['projectId' => $projectId];
        }
      }

      // Cheking member name from array with selected member name on page.
      foreach ($_SESSION['userMembers'] as $user => $member) {
        if ($user === $chose['select-user']) {
          $memberProfileId = array_key_first($member);
          $chosenTasks[$item] += ['memberProfileID' => $memberProfileId];
          $fileToPost[$item] = array_merge($chosenTasks[$item], $tasks[$item]);
        }
      }
    }
  }

  $set = 1;

  // Create utc times from html edit-text.
  foreach ($_POST['task'] as $item => $value) {
    $htmlTimeLaps[$item - 1] = $timeMetric->margeHtmlTimes($value['start-date' . $item], $value['start-time' . $item], $value['finish-date' . $item], $value['finish-time' . $item]);

    if (isset($value['select-checkbox'])) {
      array_push($htmlTimeLaps[$item - 1], $value['select-checkbox']);
    }
    else {
      array_push($htmlTimeLaps[$item - 1], 'dont send');
    }
  }

  // Sending POST request for adding timeentries.
  $step = 0;

  foreach ($htmlTimeLaps as $work => $task) {
    if ($task[0] === 'on' || $task[0] !== 'dont send') {
      $postData = new TimeMetric();
      $postTimer = $postData->postTimeEntries(
        $task['utcHtmlStart'],
        $task['utcHtmlFinish'],
        $fileToPost[$work]['select-project'],
        $fileToPost[$work]['projectId'],
        $fileToPost[$work]['descriptionText'],
        $fileToPost[$work]['memberProfileID']
      );
    }

    ++$step;
  }

  // If something wasnt selected.
  stop:
  if ($emptyProjects == TRUE or $emptyUsers == TRUE) {
    print "<div class='allert-project mx-auto' style='width: 400px;'>
		 	      <span class='message-file bg-dark'>
		            You must select all projects and users!
		        </span>
        </div>";
    // If everything was selected and sent.
  }
  else {
    print "<div class='allert-file mx-auto' style='width: 400px;'>
		 	      <span class='message-file bg-dark'>
		            All data was sent!
		        </span>
        </div>";
  }

  // Destroy all $_SESSION variables.
  session_write_close();
  session_abort();
  session_unset();
}

// Render twig template.
echo $twig->render('tmetric.html.twig', [
  'newFileName' => $newFileName,
  'twigArrays' => $_SESSION['twigVariables'],
  // From log_out.
  'activeUser' => $GLOBALS['activeUser'],
  /*
   * For using as twfig functions
   * 'csvParsedTimes' => (new TMetric\Classes\ProjectsTimes)->timeEntries(),
   * 'userMembers' => (new TMetric\Classes\TimeMetric)->accountMembers(),
   * 'projectsArray' => (new TMetric\Classes\TimeMetric)->projectInfo(),
   */
]);

<?php

/**
 * @file
 * This script controls all of routs.
 *
 * Available variables:
 * - $uri: URL of active script.
 * - $routsPath: Array of routs.
 * - $controllers: Array of controllers and there methods.
 * - check_uri(): Checking access for typed url.
 * - get_controller(): Get controller for typed url.
 * - $controllerName: Name of controller with namespace.
 * - $controllerMethod: Controlers method.
 */

declare(strict_types=1);
require_once __DIR__ . "/vendor/autoload.php";

use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\RouteCollector;

$router = new RouteCollector();

// Active uri.
$uri = $_SERVER['REQUEST_URI'];
$scriptName = $_SERVER['SCRIPT_NAME'];
$scriptFileName = $_SERVER['SCRIPT_FILENAME'];

// Array of routs.
$routsPath = [
  0 => '/',
  1 => '/login',
  2 => '/chart',
  3 => '/tmetric',
  4 => '/admission',
  5 => '/change_password',
  6 => '/log_out'
];

// Array of controllers and there methods.
$controllers = [
  '/' => ['LoginController' => 'get_Login_page'],
  '/login' => ['LoginController' => 'get_Login_page'],
  '/chart' => ['ChartController' => 'get_chart_page'],
  '/tmetric' => ['TmetricController' => 'get_tmetric_page'],
  '/admission' => ['AccessController' => 'get_access_page'],
  '/change_password' => ['ChangePasswordController' => 'get_change_password_page'],
];

/**
 * Function for checking url.
 * Check uri in routs path.
 *
 * @param string $uri
 *   Active uri.
 * @param array $routsPath
 *   Array of routs.
 *
 * @return string $url
 */
function check_uri($uri, $routsPath){
  foreach ($routsPath as $key=>$rout) {
    if ($rout == $uri) {
      $url = $rout;
      break;
    }
    else {
      $url = $routsPath[1];
    }
  }
  return $url;
}

// Check uri.
$url = check_uri($uri, $routsPath);

/**
 * Function get controller for active uri.
 *
 * @param string $url
 *   Checked uri.
 * @param array $controllers
 *   Array of controllers and there methods.
 *
 * @return array controller,method
 */
function get_controller($url, $controllers){
  foreach ($controllers as $key=>$control) {
    if ($key == $url) {
      $controller = key($control);
      $method = $control[$controller];
      break;
    } else{
      $controller = array_key_first($controllers);
      $method = array_shift($controllers);
    }
  }
return ['controller'=>$controller, 'method'=>$method];
}

// Get controller for checked uri.
$controller = get_controller($url, $controllers);

// Create full controller line with method.
$controllerName = "TMetric\Src\controllers\\" .$controller['controller'];
$getControllerMethod = get_class_methods($controllerName);
$controllerMethod = $getControllerMethod[0];

// Calling controller method for checked url.
$router->any($url, [$controllerName, $controllerMethod]);

$dispatcher = new Dispatcher($router->getData());

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = rawurldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

// Enter.
echo $dispatcher->dispatch($httpMethod, $url), "\n";

<?php

namespace TMetric\Access;

// Include log file.
ini_set('mysql_error_log', 'mysql_error_log');

/**
 * Create class for getting MYSQLI connection with db.
 *
 * @todo improve new better connection function
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class MySqliConnect {

  /**
   * Function for connect with MySqli.
   *
   * @return \mysqli
   *   $mysqli.
   */
  public function msql() {
    $servername = 'db';
    $username = 'root';
    $password = 'root';
    $dbname = 'tmetric';

    $mysqli = new \mysqli($servername, $username, $password, $dbname);

    if (!$mysqli) {
      print 'Ошибка: Невозможно установить соединение с MySQL.' . \PHP_EOL;
      print 'Код ошибки errno: ' . mysqli_connect_errno() . \PHP_EOL;
      print 'Текст ошибки error: ' . mysqli_connect_error() . \PHP_EOL;

      exit;
    }
    // For message .
    // echo "Соединение с MySQL установлено!" . PHP_EOL;
    // echo "Информация о сервере: " . mysqli_get_host_info($mysqli) . PHP_EOL;
    // mysqli_close($mysqli);
    return $mysqli;
  }

  /**
   * Function for reset MYSQLI AUTO_INCREMENT.
   *
   * @param string $table
   *   Name of msql table.
   */
  public function resetId(string $table) {
    $this->msql()->query("ALTER TABLE `{$table}` AUTO_INCREMENT = 1");
  }

  /**
   * Function for rebuild MYSQLI AUTO_INCREMENT ID.
   *
   * @param string $mysqlTable
   *   Name of msql table.
   */
  public function rebuildId(string $mysqlTable) {
    $this->msql()->query("ALTER TABLE `{$mysqlTable}` DROP `id`");
    $this->msql()->query("ALTER TABLE `{$mysqlTable}` ADD `id` BIGINT( 200 ) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)");
  }

  /**
   * Function for update MYSQLI data.
   *
   * @param string $mysqlTable
   *   Name of msql table.
   * @param string $mySqlWhatChange
   *   This mysql "row" will be = "new variable".
   * @param string $mySqlWhereChange
   *   Where msql "row" is = "msql variable".
   */
  public function updateData(string $mysqlTable,
  string $mySqlWhatChange,
                             string $mySqlWhereChange) {
    // Divide $mySqlWhatChange and $mySqlWhereChange for needed format.
    $whatwhere = $mySqlWhatChange . "=" . $mySqlWhereChange;
    $explodeUpdate = explode('=', $whatwhere);
    $this->msql()->query("UPDATE `$mysqlTable` set `$explodeUpdate[0]`='$explodeUpdate[1]' where `$explodeUpdate[2]`='{$explodeUpdate[3]}'");

  }

}

<?php

namespace TMetric\Access;

require_once "MySqliConnect.php";

/**
 * Create class for validate users.
 *
 * @todo include securety library.
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class Validation {

  /**
   * Function checking users access.
   *
   * @var $access
   *   Show users access.
   */
  public function validate() {

    // Connection to MySQLI.
    $mySqliConnect = new MySqliConnect();
    $msql = $mySqliConnect->msql();
    // Access validation.
    $access_Sql = $msql->query('SELECT * FROM `login` ')->fetch_all();

    foreach ($access_Sql as $validation => $userAccess) {
      if ($userAccess[1] === $_COOKIE['user_email'] && $_COOKIE['user_password'] === $userAccess[2]) {
        $access = 'verify';
        // If headers already sent out.
        if (headers_sent()) {
        }
        else {
          setcookie('user_access', $access);
        }
      }
    }

    if ($access !== 'verify') {
      header('location: login');
    }
  }
}
<?php

/**
 * @file
 * Displays a form for changing password.
 */

// Inclide Guzzle, bootstrap, csvparser.
require_once __DIR__ . '/../vendor/autoload.php';
// Include MENU.
require_once __DIR__ . '/../scripts/menu.php';

// Include mysqli classes.
use Twig\Extension\DebugExtension;
use TMetric\Access\MySqliConnect;
// Include twig classes.
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

// Load twig.
//$loader = new FilesystemLoader('../themes');
$loader = new FilesystemLoader('themes');
// For debugging.
$twig = new Environment($loader, ['debug' => TRUE]);
$twig->addExtension(new DebugExtension());

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

// Setup global variables.
$twig->addGlobal('post', $_POST);
$twig->addGlobal('cookie', $_COOKIE);

header('Content-type: text/html; charset=utf-8; Content-Length: 0');
// If headers already sent out.
if (headers_sent()) {
}
else {
}

// Access validation.
$validate = new \TMetric\Access\Validation();
$validate->validate();

// Change password.
if (isset($_POST['changePassw'])) {
  $newPassword = crypt($_POST['newPassword'], 'tmetric');
  $mySqliConnect->updateData("login", "password=" . $newPassword, "email=" . $_COOKIE['changeFor']);
  clearstatcache();
  unset($_COOKIE['changeFor'], $_COOKIE['changePassword']);

  setcookie('changeFor', NULL, -1, '/');
  setcookie('changePassword', NULL, -1, '/');
  header('location: admission');

  exit;
}

$temple = $twig->load('change_password.html.twig');
echo $temple->render([
// From log_out.
  'activeUser' => $passActiveUser,
]);

<?php

namespace TMetric\Classes;

// Include parseCSV class.
require __DIR__ . '/../vendor/autoload.php';

/**
 * @file
 * Parse Jira and Clockify csv files.
 *
 * Available classes:
 * - CSVImporter: Contains SQL access for users.
 * - ProjectsTimes: Contains current user mail.
 * - ParseDurationTime: Contains current user mail.
 */

use ParseCsv\Csv;

/**
 * Create class for parsing jira and clockify files.
 *
 * Use parsing function for getting csv data in likely format.
 *
 * @todo improve format of parsing, phone view
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class CSVImporter {

  /**
   * Function parse csv file.
   *
   * @return array
   *   $csvFileArray.
   */
  public function csvParseFile() {
    // Create new parseCSV object.
    $csv = new Csv();
    $fileLocation = $_SESSION['file_location'];
    // Its a example of using csvparse class.
    // $csvFile = new ParseCsv\Csv($fileLocation);
    // $csv->encoding('UTF-16', 'UTF-8');
    // $csv->delimiter = "\t"; .
    $csv->parse($fileLocation);

    return $csvFileArray = ($csv->data);
  }

  /**
   * Function extract first array value, headers.
   *
   * @return array
   *   $headersFile.
   */
  public function headersFile() {
    $csv_Array = $this->csvParseFile();
    // Get first element of array.
    $first_element = array_shift($csv_Array);
    return $headersFile = array_keys($first_element);
  }

}

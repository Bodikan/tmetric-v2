<?php

namespace TMetric\Classes;

require __DIR__ . '/../vendor/autoload.php';

/**
 * Create class for getting duration time in hours and minutes.
 *
 * Use time in hour format, reformat it in "HH:II:SS".
 * Get summary durations.
 *
 * @todo improve using better logic and shortly format
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class ParseDurationTime {

  /**
   * Function for get duration task with title of tag.
   *
   * @param array $projectTagsArray
   *   Tags names.
   * @param array $taskDurations
   *   Tags durations.
   *
   * @return array
   *   $musDurations
   */
  public function getDurationWithLabelTag(array $projectTagsArray, array $taskDurations) {
    foreach ($projectTagsArray as $key => $prj) {
      $musDurations[$key] = [$prj => $taskDurations[$key]];
    }

    return $musDurations;
  }

  /**
   * Function for get duration task times.
   *
   * Get utc and simple time from inhours format.
   *
   * @param array $taskMinutes
   *   Time task in minutes.
   *
   * @return array
   *   taskDurations, taskDurationsString.
   */
  public function getDurationWithMinutes(array $taskMinutes) {

    // отримуєм з масиву що містить хв, масив з год і хв по таскам.
    foreach ($taskMinutes as $index => $taskTime) {
      if ($taskTime >= 60) {
        if (mb_strlen((int) ($taskTime / 60)) < 2) {
          $h = '0' . (int) ($taskTime / 60);
        }
        else {
          $h = (int) ($taskTime / 60);
        }

        $explmm = explode('.', round($taskTime / 60, 2));

        if (isset($explmm[1])) {
          $mm = '0.' . $explmm[1];
        }
        else {
          $mm = 0;
        }

        if ($mm === 0) {
          $m = '00';
        }
        else {
          if (mb_strlen((int) ($mm * 60)) < 2) {
            $m = '0' . (int) ($mm * 60);
          }
          else {
            $m = (int) ($mm * 60);
          }
        }
      }
      else {
        $h = '00';

        if (mb_strlen((int) $taskTime) < 2 && $taskTime !== 0) {
          $m = '0' . $taskTime;
        }
        elseif ($taskTime === 0) {
          $m = '00';
        }
        else {
          $m = $taskTime;
        }
      }

      $taskDurations[] = $h . ':' . $m . '';
      $taskDurationsString[] = $h . 'h:' . $m . 'm';
    }

    return [
      'taskDurations' => $taskDurations,
      'taskDurationsString' => $taskDurationsString,
    ];
  }

  /**
   * Function summary times for all groups of tags.
   *
   * @param array $musDurations
   *   Using array of durations.
   *
   * @return array
   *   [$tagsDuration = $tagsDurationReverse];
   */
  public function getSummaryDurationTime(array $musDurations) {

    // Получаїм масив з масивами тегів та їх масивами часу.
    foreach ($musDurations as $key => $tag) {
      $tagKey = array_key_first($tag);

      if (\array_key_exists($tagKey, $tag)) {
        if (empty($tagsDuration) || !isset($tagsDuration[$tagKey])) {
          $tagsDuration[$tagKey] = [$key => $tag[$tagKey]];
        }
        else {
          $tagsDuration[$tagKey] += [$key => $tag[$tagKey]];
        }
      }
    }
    // Set arrays key from 0 to ++ перебудовуєм індекси масивів з 0 до N.
    foreach ($tagsDuration as $tag => $values) {
      $n = 0;

      foreach ($values as $key => $value) {
        $tagsDurationReverse[$tag][] = $value;
        ++$n;
      }
    }

    return $tagsDuration = $tagsDurationReverse;
  }

}

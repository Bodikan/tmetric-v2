<?php

namespace TMetric\Classes;

use GuzzleHttp\Client;
// Exception is thrown in the event of a networking error.
use GuzzleHttp\Exception\RequestException;

/**
 * Create class for getting tmetric date.
 *
 * @see https://app.tmetric.com/help
 */
class TimeMetric {

  /**
   * Define $userProfile.
   *
   * Переменная private $userProfile = 'userprofile';
   *
   * @var string
   */
  const USERPROFILE = 'userprofile';

  /**
   * Function for get account info.
   *
   * @var $path
   *   using getPathInfo() for getting pathes array
   *
   * @return array
   *   $accountInfo.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function accountInfo() {
    $path = $this->getPathInfo();

    return $accountInfo = $this->useGazzle('GET', $path['accountInfo'], '');
  }

  /**
   * Function for get account members.
   *
   * @var $accountInfo
   *   array with user members
   *
   * @return array
   *   $userMembers.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function accountMembers() {
    // Creating user_members array with names and ids.
    $accountInfo = $this->accountInfo();

    for ($i = 0; $i < count($accountInfo['members']); ++$i) {
      $memberProfileId = $accountInfo['members'][$i]['userProfileId'];
      $memberName = $accountInfo['members'][$i]['userName'];
      $userMembers[$memberName] = [$memberProfileId => $memberName];
    }

    return $userMembers;
  }

  /**
   * IS NOT USED NOW!
   *
   * Function for checking selected users and projects
   * with relevant users and projects.
   *
   * тут ми перевіряємо масив обраних користувачів та проектів з масивами
   * дійсних на даний момент користувачами та проектами в tmetric.
   * $projectsArray та $usersArray вказуєм як параметр масив обраних
   * користувачів та проектів, або один з них.
   *
   * @param array $projectsArray
   *   Array with projects.
   * @param array $usersArray
   *   Array with users.
   *
   * @return array
   *   checkProjects,checkMembers.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function checking(array $projectsArray, array $usersArray) {
    $projectInfo = $this->projectInfo();

    foreach ($projectInfo as $key => $proj) {
      $projectId = $proj['projectId'];
      $projectName = $proj['projectName'];
      $projects[] = $projectName;
    }

    $members = $this->membersName();

    // якщо параметр $projectsArray не пустий.
    // якщо тільки параметр $usersArray пустий.
    if (empty($usersArray) || $usersArray === 'no' || !isset($usersArray)) {

      // Checking for deleted or added users.
      $checkProjects = $this->checkItems($projects, $projectsArray);
      $checkMembers = $usersArray;

      // якщо параметр $usersArray не пустий.
      // якщо тільки параметр $projectsArray пустий.
    }
    elseif (empty($projectsArray) || $projectsArray === 'no' || !isset($projectsArray)) {
      $checkMembers = $this->checkItems($members, $usersArray);
      $checkProjects = $projectsArray;

      // якшо обидва параметри функції не пусті то перевіряємо обидва.
    }
    elseif (!empty($usersArray) && !empty($projectsArray) && ($usersArray !== 'no') && ($projectsArray !== 'no')) {
      $checkProjects = $this->checkItems($projects, $projectsArray);
      $checkMembers = $this->checkItems($members, $usersArray);
    }
    else {
      print 'you didnt call all parameters';
    }

    return ['checkProjects' => $checkProjects, 'checkMembers' => $checkMembers];
  }

  /**
   * IS NOT USED NOW!
   *
   * Function for checking selected users and projects with relevant users and
   * projects.
   *
   * @param array $countCurrentItems
   *   Actual users and projects.
   * @param array $countChosenItems
   *   Selected users and projects.
   *
   * @return string
   *   "checked or no".
   */
  public function checkItems(array $countCurrentItems, array $countChosenItems) {
    // так як на місце видаленого проекту можна добавити інший
    // і кількість не зміниться.
    if (count($countCurrentItems) < count($countChosenItems)) {
      $chekedItem = array_diff($countChosenItems, $countCurrentItems);
      $res = array_values($chekedItem);

      foreach ($res as $key => $item) {
        $result[] = $item . 'has been deleted, pls reparse the file';
      }
    }
    elseif (count($countCurrentItems) > count($countChosenItems)) {
      $chekedItem = array_diff($countCurrentItems, $countChosenItems);
      $res = array_values($chekedItem);

      foreach ($res as $key => $item) {
        $result[] = $item . ' has been added, pls reparse the file';
      }
    }

    // якшо кількість рівна - все ок, інакше повертаємо повідомлення.
    if (empty($result)) {
      return $result = 'cheked is fine';
    }

    return $result;
  }

  /**
   * Function get string pathes for creating GET requests.
   *
   * @var $getInfo
   *   Using profileInfo() for getting user data.
   *
   * @return array|string
   *   $getPathArray
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPathInfo() {
    $getInfo = $this->profileInfo();
    $userAccountId = $getInfo['activeAccountId'];
    $userProfileId = $getInfo['userProfileId'];

    return $getPathArray = [
      'accountInfo' => 'accounts/' . $userAccountId,
      'accountScopeInfo' => 'accounts/' . $userAccountId . '/scope',
      'projectsInfo' => 'accounts/' . $userAccountId . '/projects',
      'TimeOffPolicies' => 'accounts/' . $userAccountId . '/timeoff/policies',
      'TimeOffBalances' => 'accounts/' . $userAccountId . '/timeoff/balances',
      'TimeOffBalancesMovements' => 'accounts/' . $userAccountId . '/timeoff/balances/movements',
      'TimeOffRequests' => 'accounts/' . $userAccountId . '/timeoff/balances/movements',
      'TimeEntriesGroup' => 'accounts/' . $userAccountId . '/timeentries/group?useUtcTime=true&timeRange.startTime=',
      'TimeEntriesUserProfileID' => 'accounts/' . $userAccountId . '/timeentries/',
      'ReportDetailed' => 'reports/detailed?reportParams.useUtcTime=true&reportParams.accountId=' . $userAccountId . '&reportParams.startDate=',
    ];
  }

  /**
   * Function for getting datailed report.
   *
   * @param string $firstDay
   *   First day from html datetime filter.
   * @param string $lastDay
   *   Last day from html datetime filter.
   *
   * @return array
   *   $getTimeEntriesReport
   *   Return array with selected filter data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTimeEntriesDetailedReport(string $firstDay, string $lastDay) {
    $ex1 = explode('-', $firstDay);
    $startDay = $ex1[2];
    // Declare month number and initialize it.
    $monthNum = $ex1[1];
    // Create date object to store the DateTime format.
    $dateObj = \DateTime::createFromFormat('!m', $monthNum);
    // Store the month name to variable.
    $startMonth = $dateObj->format('F');
    $startYear = $ex1[0];
    $ex2 = explode('-', $lastDay);
    $endtDay = $ex2[2];
    // Declare month number and initialize it.
    $month_num = $ex2[1];
    // Use mktime() and date() function to.
    // convert number to month name.
    $endtMonth = date('F', mktime(0, 0, 0, $month_num, 10));
    $endYear = $ex2[0];
    $pathArray = $this->getPathInfo();
    $going = $pathArray['ReportDetailed'] . $startDay . '%20' . $startMonth . '%20' . $startYear . '%2C%2001%3A00%3A00&reportParams.endDate=' . $endtDay . '%20' . $endtMonth . '%20' . $endYear . '%2C%2023%3A59%3A00';

    return $getTimeEntriesReport = $this->useGazzle('GET', $going, '');
  }

  /**
   * IS NOT USED NOW!
   *
   * Function for getting time report for all users and for selected time
   * filter.
   *
   * @param string $startDate
   *   Start date from html datetime filter.
   * @param string $endDate
   *   End date from html datetime filter.
   *
   * @return array
   *   $getTimeEntriesGroup
   *   Return array with selected filter data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTimeEntriesGroup(string $startDate, string $endDate) {
    $ex1 = explode('-', $startDate);
    $startDay = $ex1[2];
    // Declare month number and initialize it.
    $monthNum = $ex1[1];
    // Create date object to store the DateTime format.
    $dateObj = \DateTime::createFromFormat('!m', $monthNum);
    // Store the month name to variable.
    $startMonth = $dateObj->format('F');
    $startYear = $ex1[0];
    $ex2 = explode('-', $endDate);
    $endtDay = $ex2[2];
    // Declare month number and initialize it.
    $month_num = $ex2[1];
    // Use mktime() and date() function to.
    // Convert number to month name.
    $endtMonth = date('F', mktime(0, 0, 0, $month_num, 10));
    $endYear = $ex2[0];
    $path = $this->getPathInfo();
    $going = $path['TimeEntriesGroup'] . $startDay . '%20' . $startMonth . '%20' . $startYear . '%2C%2001%3A00%3A00&timeRange.endTime=' . $endtDay . '%20' . $endtMonth . '%20' . $endYear . '%2C%2023%3A59%3A00';

    return $getTimeEntriesGroup = $this->useGazzle('GET', $going, '');
  }

  /**
   * IS NOT USED NOW!
   *
   * Function for getting info with using Guzzle.
   *
   * @param string $get_path
   *   Changing part of URL path.
   *
   * @return array|mixed
   *   json_decode GET requests.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTmetricInfo(string $get_path) {
    $token = (string) ($this->token());
    $client = $this->client = new Client(['base_uri' => 'https://app.tmetric.com/api/']);
    $headers = [
      'Authorization' => 'Bearer ' . $token,
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
    ];
    $response = $client->request('GET', $get_path, [
      'headers' => $headers,
    ])->getBody()->getContents();

    return json_decode($response, 1);
  }

  /**
   * Function for marge two time entries (for html times array).
   *
   * @param string $day1
   *   Day start.
   * @param string $timeLap1
   *   Time start.
   * @param string $day2
   *   Day finish.
   * @param string $timeLap2
   *   Time finish.
   *
   * @return array
   *   utc (start, end).
   *
   * @throws \Exception
   */
  public function margeHtmlTimes($day1, $timeLap1, $day2, $timeLap2) {
    $margedStartTime = new \DateTime($day1 . ' ' . $timeLap1);
    // If U need common time format use this: format("Y-m-d H:i:s"); .
    $utcStartTimeLap = $margedStartTime->format('Y-m-d\TH:i:s');

    $margedFinishTime = new \DateTime($day2 . ' ' . $timeLap2);
    // If U need common time format use this: format("Y-m-d H:i:s"); .
    $utcFinishTimeLap = $margedFinishTime->format('Y-m-d\TH:i:s');

    return [
      'utcHtmlStart' => $utcStartTimeLap,
      'utcHtmlFinish' => $utcFinishTimeLap,
    ];
  }

  /**
   * Function for get get users names.
   *
   * @var = accountMembers()
   *   array with user members
   *
   * @return array
   *   $members with names.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function membersName() {
    $userMembers = $this->accountMembers();

    foreach ($userMembers as $key => $member) {
      $memberId = array_key_first($member);
      $memberName = $member[$memberId];
      $members[] = $memberName;
    }

    return $members;
  }

  /**
   * Function get string pathes for creating POST requests.
   *
   * @var
   *   using profileInfo() for getting user data.
   *
   * @return array|string
   *   $postPathArray.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function postPathInfo() {
    $getInfo = $this->profileInfo();
    $userAccountId = $getInfo['activeAccountId'];
    $userProfileId = $getInfo['userProfileId'];

    return $postPathArray = [
      'postTimeEntries' => 'accounts/' . $userAccountId . '/timeentries/',
    ];
  }

  /**
   * Function for sending timeentries data into tmetric API account.
   *
   * @param string $startProject
   *   Start date in UTC format.
   * @param string $endProject
   *   End date in UTC format.
   * @param string $projectName
   *   Name of project.
   * @param string $projectId
   *   Project ID.
   * @param string $description
   *   Project description.
   * @param string $memberID
   *   Member ID whom sending data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function postTimeEntries(string $startProject, string $endProject, string $projectName, string $projectId, string $description, string $memberID) {
    $memberProfileID = (string) $memberID;
    $data = [
      'endTime' => $endProject,
      'startTime' => $startProject,
      'isBillable' => TRUE,
      'isInvoiced' => FALSE,
      // "timeEntryId"     => rand(1,777777),
      'timeEntryId' => 0,
      'details' => [
        'description' => $description,
        'projectId' => $projectId,
      ],
      'projectName' => $projectName,
      'tagsIdentifiers' => [],
    ];
    $path = $this->postPathInfo();
    // Use return $this->useGazzle.
    $this->useGazzle('POST', $path['postTimeEntries'] . $memberProfileID, $data);
  }

  /**
   * IS NOT USED NOW!
   *
   * Function for POST data with using Guzzle.
   *
   * @param array $data
   *   Changing part of URL path.
   * @param string $post_path
   *   Changing part of URL path.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function postTmetricInfo(array $data, string $post_path) {
    $client = $this->client = new Client(['base_uri' => 'https://app.tmetric.com/api/']);
    $token = (string) ($this->token());
    $headers = [
      'Authorization' => 'Bearer ' . $token,
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
    ];
    $post = $client->request('POST', $post_path, [
      'headers' => $headers,
      'json' => [
        $data,
      ],
    ]);
  }

  /**
   * Function for get profile info.
   *
   *   Method = GET, Path = $userProfile.
   *   return $profileInfo = $this->useGazzle('GET', $this->userProfile, '');
   *
   * @return array
   *   $profileInfo.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function profileInfo() {
    return $profileInfo = $this->useGazzle('GET', self::USERPROFILE, '');
  }

  /**
   * Function for get projects info.
   *
   * @var
   *   using getPathInfo() for getting pathes array
   *
   * @return array
   *   $projectInfo with projects.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function projectInfo() {
    $path = $this->getPathInfo();

    return $projectInfo = $this->useGazzle('GET', $path['projectsInfo'], '');
  }

  /**
   * Function for get account token from YML file.
   *
   * @return string|token
   *   Token.
   */
  public function token() {
    $yaml = file('access/auth.yaml');
    $yamlToken = $yaml['10'];
    $stringArray = explode(': ', $yamlToken);

    return $token = trim($stringArray[1]);
  }

  /**
   * Function for get post requests with using Guzzle.
   *
   * @param string $method
   *   GET|POST.
   * @param string $path
   *   Path here send the data.
   * @param mixed $data
   *   The data to send.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @return array|mixed
   *   The response array.
   */
  public function useGazzle(string $method, string $path, $data) {
    $token = (string) ($this->token());

    $client = new Client([
      'base_uri' => 'https://app.tmetric.com/api/',
      'headers' => [
        'Authorization' => 'Bearer ' . $token,
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
      ],
    ]);

    if ($method === 'POST' && !empty($data)) {
      try {
        $response = $client->request($method, $path, [
          'json' => $data,
        ]);
        if ($response) {
          return json_decode($response->getBody());
        }
      }
      // Вивод ошибок guzzle requests.
      catch (RequestException $e) {
        if ($e->hasResponse()) {
          echo $e->getMessage();
        }
      }
    }
    elseif ($method === 'GET' && !empty($data)) {
      $response = $client->request($method, $path, [
        'json' => $data,
      ])->getBody()->getContents();

      return json_decode($response, 1);
    }
    elseif ($method === 'GET' && empty($data)) {
      $response = $client->request($method, $path, []);

      return json_decode($response->getBody()->getContents(), 1);
    }
  }

  /**
   * Function for using array_key_first in twig.
   *
   * @var $key
   *   using array_key_first() for getting array first key
   *
   * @return mixed
   *   $key.
   */
  public function firstKey($array) {

    $firstKey = array_key_first($array);

    return $firstKey;
  }

}

<?php

namespace TMetric\Classes;

require __DIR__ . '/../vendor/autoload.php';

/**
 * Create class for getting times array to fill the post query.
 *
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class ProjectsTimes extends CSVImporter {

  /**
   * Define $csvImporter.
   *
   * Cюда запишется объект класса CSVImporter.
   *
   * @var array
   */
  private $csvImporter;

  /**
   * Конструктор класса CSVImporter для доступа к функции csvParseFile() .
   *
   * Запишем объект вспомогательного класса в свойство.
   *   csvImporter->csvParseFile().
   */
  public function __construct() {
    $this->csvImporter = new CSVImporter();
  }

  /**
   * Function uses csvParseFile from CSVImporter.
   *
   * @return array
   *   csvParseFile().
   */
  public function csvParseFile() {
    return $this->csvImporter->csvParseFile();
  }

  /**
   * Function use headersFile from CSVImporter..
   *
   * @return array
   *   headersFile().
   */
  public function headersFile() {
    return $this->csvImporter->headersFile();
  }

  /**
   * Function for get interval time.
   *
   * Get utc and simple time from inhours format.
   *
   * @param mixed $taskHours
   *   Minutes task in hours format.
   *
   * @return mixed
   *   interval, duration.
   *
   * @throws \Exception
   */
  public function getTaskInterval($taskHours) {
    $inHours = $taskHours;
    // Берем минуты в часах, если они есть то + "0" = 0.5; для преобразования
    // в минуты 0.5*60=30мин.
    $minInHoursArr = explode('.', $inHours);

    if (!isset($minInHoursArr[1])) {
      $minInHours = '00';
    }
    else {
      $minInHours = (float) ('0.' . $minInHoursArr[1]);
    }
    // берем часи из времени, если часов меньше 1 то + "0" = 01; 03; 08.
    if (mb_strlen(current(explode('.', $inHours))) < 2) {
      $hours = '0' . current(explode('.', $inHours));
    }
    else {
      $hours = current(explode('.', $inHours));
    }
    // Берем минуты из времени, если во времени были секунды, также получаем
    // остаток секунды.
    $minSec = explode('.', (round($minInHours * 60, 2)));

    if (!isset($minSec[0]) || (int) ($minSec[0]) === 0) {
      $minutes = '0' . $minSec[0];
    }
    else {
      $minutes = current($minSec);
    }
    // берем секунды из времени, если пусто то = 0, если < 2 символов то +"0".
    if (!isset($minSec[1])) {
      $seconds = '00';
    }
    elseif (mb_strlen($minSec[1]) < 2) {
      $seconds = $minSec[1] . '0';
    }
    else {
      $seconds = next($minSec);
    }
    $interval = new \DateInterval('PT' . $hours . 'H' . $minutes . 'M' .
      $seconds . 'S');
    $duration = ($hours . ':' . $minutes . ':' . $seconds);

    return ['interval' => $interval, 'duration' => $duration];
  }

  /**
   * Function create array with times data.
   *
   * @return array
   *   csvParseFile().
   *
   * @throws \Exception
   */
  public function timeEntries() {
    $csvArray = $this->csvParseFile();

    // JUST FOR EXAMPLE.
    $headersFile = $this->headersFile();

    /**
     * IS NOT USED NOW!
     * Function for get parsed date and time from task..
     *
     * @return array
     *   $startArray.
     * @throws \Exception
     */
    function getTaskDate($startTaskDate) {
      $startTaskDate = new \DateTime($startTaskDate);
      $startWorkTask = $startTaskDate->format('Y-m-d H:i:s');

      return $startArray = explode(' ', trim($startWorkTask));
    }

    $n = 0;
    $step = 0;

    $csvArray = array_reverse($csvArray);

    foreach ($csvArray as $row => $task) {
      $fileLocation = $_SESSION['file_location'];

      /*
       * Если в файле есть название jira.
       * @uses $fileJira
       */
      if (preg_match('/\\bjira\\b/i', $fileLocation)) {
        $startDate = current(explode(' ', $task['Work date']));
        $description = $task['Work Description'];

        if ($step === 0) {
          $startTime = '00:01:00';
          $startTaskArray = new \DateTime($startDate . ' ' . $startTime);
          $startTask = $startTaskArray->format('Y-m-d H:i:s');
          $utcStartTask = $startTaskArray->format('Y-m-d\TH:i:s');
          $getDurationsArray = $this->getTaskInterval($task['Hours']);
          $duration = $getDurationsArray['duration'];

          $endtDate = $startDate;
          $endTaskArray = $startTaskArray->add($getDurationsArray['interval']);
          $endTask = $endTaskArray->format('Y-m-d H:i:s');
          $endTime = $endTaskArray->format('H:i:s');
          $utcEndTask = $endTaskArray->format('Y-m-d\TH:i:s');
        }
        elseif ($step > 0) {
          $prevDate = current(explode(' ', $csvArray[$step - 1]['Work date']));

          if ($startDate === $prevDate) {
            $startTaskArray = new \DateTime($_SESSION['fileTasksArray'][$step - 1]['endDate'] . ' ' . $_SESSION['fileTasksArray'][$step - 1]['endTime']);
            $startTime = $_SESSION['fileTasksArray'][$step - 1]['endTime'];
            $startTask = $_SESSION['fileTasksArray'][$step - 1]['endTimeTask'];
            $utcStartTask = $_SESSION['fileTasksArray'][$step - 1]['utcEndTime'];
            $getDurationsArray = $this->getTaskInterval($task['Hours']);
            $duration = $getDurationsArray['duration'];
            $endtDate = $startDate;
            $endTaskArray = $startTaskArray->add($getDurationsArray['interval']);
            $endTask = $endTaskArray->format('Y-m-d H:i:s');
            $endTime = $endTaskArray->format('H:i:s');
            $utcEndTask = $endTaskArray->format('Y-m-d\TH:i:s');
          }
          else {
            $startTime = '00:01:00';
            $startTaskArray = new \DateTime($startDate . ' ' . $startTime);
            $startDate = current(explode(' ', $task['Work date']));
            $startTask = $startTaskArray->format('Y-m-d H:i:s');
            $utcStartTask = $startTaskArray->format('Y-m-d\TH:i:s');
            $getDurationsArray = $this->getTaskInterval($task['Hours']);
            $duration = $getDurationsArray['duration'];
            $endTaskArray = $startTaskArray->add($getDurationsArray['interval']);
            $endtDate = $startDate;
            $endTask = $endTaskArray->format('Y-m-d H:i:s');
            $endTime = $endTaskArray->format('H:i:s');
            $utcEndTask = $endTaskArray->format('Y-m-d\TH:i:s');
          }
        }

        /*
         * Иначе, если в файле есть название clockify.
         * @uses $fileClockify
         */
      }
      elseif (preg_match('/\\bclockify\\b/i', $fileLocation)) {
        $description = $task['Description'];
        $startDate = $task['Start Date'];

        if ($step === 0) {
          $startTime = '00:01:00';
          $startTaskArray = new \DateTime($startDate . ' ' . $startTime);
          $startTask = $startTaskArray->format('Y-m-d H:i:s');
          $utcStartTask = $startTaskArray->format('Y-m-d\TH:i:s');
          $duration = $task['Duration (h)'];
          $endtDate = $task['End Date'];

          if ($startDate !== $endtDate) {
            $endtDate = $startDate;
          }
          $timeInterval = strtotime($startTime) + strtotime($duration) - strtotime('00:00:00');
          $endTime = date('H:i:s', $timeInterval);
          $endTaskArray = new \DateTime($endtDate . ' ' . $endTime);
          $endTask = $endTaskArray->format('Y-m-d H:i:s');
          $utcEndTask = $endTaskArray->format('Y-m-d\TH:i:s');
        }
        elseif ($step > 0) {
          // Додавати, вносити і перезаписувати попередній елемент,
          // а якшо ні то просто добавляти.
          $prevDate = $csvArray[$step - 1]['Start Date'];
          if ($startDate === $prevDate) {
            $startTime = $_SESSION['fileTasksArray'][$step - 1]['endTime'];
            $startTaskArray = new \DateTime($startDate . ' ' . $startTime);
            $startTask = $startTaskArray->format('Y-m-d H:i:s');
            $utcStartTask = $startTaskArray->format('Y-m-d\TH:i:s');
            $duration = $task['Duration (h)'];
            $endtDate = $task['End Date'];

            if ($startDate !== $endtDate) {
              $endtDate = $startDate;
            }
            $timeInterval = strtotime($startTime) + strtotime($duration) - strtotime('00:00:00');
            $endTime = date('H:i:s', $timeInterval);
            $endTaskArray = new \DateTime($endtDate . ' ' . $endTime);
            $endTask = $endTaskArray->format('Y-m-d H:i:s');
            $utcEndTask = $endTaskArray->format('Y-m-d\TH:i:s');
          }
          else {
            $startTime = '00:01:00';
            $startTaskArray = new \DateTime($startDate . ' ' . $startTime);
            $startTask = $startTaskArray->format('Y-m-d H:i:s');
            $utcStartTask = $startTaskArray->format('Y-m-d\TH:i:s');
            $duration = $task['Duration (h)'];
            $endtDate = $task['End Date'];

            if ($startDate !== $endtDate) {
              $endtDate = $startDate;
            }
            $timeInterval = strtotime($startTime) + strtotime($duration) - strtotime('00:00:00');
            $endTime = date('H:i:s', $timeInterval);
            $endTaskArray = new \DateTime($endtDate . ' ' . $endTime);
            $endTask = $endTaskArray->format('Y-m-d H:i:s');
            $utcEndTask = $endTaskArray->format('Y-m-d\TH:i:s');
          }
        }
      }

      $fileTasksArray[] = [
        'startDate' => $startDate,
        'startTime' => $startTime,
        'duration' => $duration,
        'endDate' => $endtDate,
        'endTime' => $endTime,
        'startHeadertTitle' => 'Date time start',
        'endHeaderTitle' => 'Date time end',
        'descriptionTitle' => 'Work Description',
        'descriptionText' => $description,
        'startTimeTask' => $startTask,
        'endTimeTask' => $endTask,
        'utcStartTime' => $utcStartTask,
        'utcEndTime' => $utcEndTask,
      ];
      ++$step;
      $_SESSION['fileTasksArray'] = $fileTasksArray;
    }

    // Get adding durations in one day times, for getting one day seconds sum.
    foreach ($fileTasksArray as $key => $task) {
      if ($key === 0) {
        $interval = $task['duration'];
        $oneDaySecondsArray[] = $interval;
        $_SESSION['oneDaySecondsArray'] = $oneDaySecondsArray;
      }
      else {
        if ($task['startDate'] === $fileTasksArray[$key - 1]['startDate']) {
          ++$n;
          $oneDayIntervalSumma = strtotime($task['duration']) + strtotime($_SESSION['oneDaySecondsArray'][$key - $n]) - strtotime('00:00:00');
          $interval = date('H:i:s', $oneDayIntervalSumma);
          $oneDaySecondsArray[$key - $n] = $interval;
          $_SESSION['oneDaySecondsArray'][$key - $n] = $interval;
        }
        else {
          $m = \count($_SESSION['oneDaySecondsArray']);
          $interval = $task['duration'];
          $oneDaySecondsArray[$m] = $interval;
          $_SESSION['oneDaySecondsArray'][$m] = $interval;
        }
      }
    }

    // Adding seconds from all days.
    foreach ($oneDaySecondsArray as $key => $oneDayDuration) {
      $exploвeDayDuration = explode(':', $oneDayDuration);
      $ss[] = $exploвeDayDuration[2];
    }
    $secondsSum = round(array_sum($ss) / 60, 2);

    // Making duration from $secondsSum in 00:mm:00 to post.
    $explSecondsDuration = current(explode('.', $secondsSum));

    if (mb_strlen($explSecondsDuration) < 2) {
      $explSecondsDuration = '0' . $explSecondsDuration;
    }
    else {
      $explSecondsDuration = $explSecondsDuration;
    }
    $secondsDuration = '00:' . $explSecondsDuration . ':00';

    // Making array summ seconds and
    // add to $fileTasksArray, $_SESSION['fileTasksArray'].
    $startTimeSeconds = '00:01:00';
    $lastItem = end($fileTasksArray);
    $dataOfFile = new \DateTime($lastItem['endDate']);
    $lastSaturday = $dataOfFile->modify('last Saturday of')->format('Y-m-d');
    $startTaskSeconds = date('Y-m-d H:i:s', strtotime($lastSaturday . ' ' . $startTimeSeconds));
    $utcStartTaskSeconds = date('Y-m-d\TH:i:s', strtotime($lastSaturday . ' ' . $startTimeSeconds));
    $taskIntervalSeconds = date('H:i:s', strtotime($startTimeSeconds) + strtotime($secondsDuration) -
      strtotime('00:00:00'));
    $endTaskSeconds = date('Y-m-d H:i:s', strtotime($lastSaturday . ' ' . $taskIntervalSeconds));
    $utcEndTaskSeconds = date('Y-m-d\TH:i:s', strtotime($lastSaturday . ' ' .
      $taskIntervalSeconds));
    $secondsToPost = [
      'startDate' => $lastSaturday,
      'startTime' => $startTimeSeconds,
      'duration' => $secondsDuration,
      'endDate' => $lastSaturday,
      'endTime' => $taskIntervalSeconds,
      'startHeadertTitle' => 'Date time start',
      'endHeaderTitle' => 'Date time end',
      'descriptionTitle' => 'Work Description',
      'descriptionText' => 'Count of seconds',
      'startTimeTask' => $startTaskSeconds,
      'endTimeTask' => $endTaskSeconds,
      'utcStartTime' => $utcStartTaskSeconds,
      'utcEndTime' => $utcEndTaskSeconds,
    ];
    array_push($fileTasksArray, $secondsToPost);
    array_push($_SESSION['fileTasksArray'], $secondsToPost);

    return $fileTasksArray;
  }

}

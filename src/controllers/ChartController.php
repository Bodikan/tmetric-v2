<?php


namespace TMetric\Src\controllers;

/**
 * Create controller class for route /chart.
 *
 * @todo include securety library.
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class ChartController {

  /**
   * Function include php logic for /chart route.
   */
  public function get_chart_page() {

    require_once "scripts/chart.php";

  }

}
<?php


namespace TMetric\Src\controllers;

/**
 * Create controller class for route /tmetric.
 *
 * @todo include securety library.
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class TmetricController {

  /**
   * Function include php logic for /tmetric route.
   */
  public function get_tmetric_page() {

    require_once "scripts/tmetric.php";

  }

}
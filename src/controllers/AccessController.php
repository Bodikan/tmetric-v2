<?php


namespace TMetric\Src\controllers;

/**
 * Create controller class for route /access.
 *
 * @todo include securety library.
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class AccessController {

  /**
   * Function include php logic for /access route.
   */
  public function get_access_page() {

    require_once "scripts/access.php";

  }

}
<?php


namespace TMetric\Src\controllers;

/**
 * Create controller class for route /change_password.
 *
 * @todo include securety library.
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class ChangePasswordController {

  /**
   * Function include php logic for /change_password route.
   */
  public function get_change_password_page() {

    require_once "access/change_password.php";

  }

}
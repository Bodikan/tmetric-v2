<?php


namespace TMetric\Src\controllers;

/**
 * Create controller class for route /login.
 *
 * @todo include securety library.
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class LoginController {

  /**
   * Function include php logic for /login route.
   */
  public function get_Login_page() {

    require_once "scripts/login.php";

  }

}
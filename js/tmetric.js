
/**
 * Function watch for selected users and projects at html "options"
 * and change other options depended from user choice.
 *
 * @author bod UA <godinmyhurt@gmail.com>
 * @version 1.0
 * @copyright Copyright (c) 2020, bod UA
 *
 * @var curentElement
 *    Get active element.
 * @var curentElementClassName
 *    Get active element by the class.
 * @var curentPosition
 *    Get active element number ID.
 * @var countElement
 *    Count elements by tags name.
 */
function changeOptions() {
    var curentElement = document.activeElement;
    var curentElementClassName = curentElement.className;
    var curentPosition = Number.parseInt(curentElementClassName);
    var countElement = document.getElementsByTagName("fieldset").length;

    for (var i = curentPosition; i <= countElement; i++) {
        document.getElementById('select-project' + i).value = document.getElementById('select-project' + curentPosition).value;
    }
    for (var j = curentPosition; j <= countElement; j++) {
        document.getElementById('select-user' + j).value = document.getElementById('select-user' + curentPosition).value;
    }
}


/**
 * Function watch for html durations
 * and it changes other time depended for changed duration.
 *
 * @author bod UA <godinmyhurt@gmail.com>
 * @version 1.0
 * @copyright Copyright (c) 2020, bod UA
 *
 * @var countElement
 *    Count elements by tags name.
 * @function strToTime
 *    Edit string text in datetime format "00:00:00".
 *    @return newTime
 * @function timeSum()
 *    Adding two times.
 *    @return hours + ':' + minutes + ':' + seconds
 */
function checkHtmlTime() {

    var countelement = document.getElementsByTagName("fieldset").length;
    // Цикл обробки усіх записів.
    for (var i = 1; i <= countelement; i++) {

        // Функція переводить строку в дату.
        function strToTime(strTime) {

            var dateTime = moment(`${strTime}`, 'HH:mm:ss').format();
            var newDateTime = new Date(dateTime);
            var hrs = newDateTime.getHours();
            var min = newDateTime.getMinutes();
            var sec = newDateTime.getSeconds();
            if (hrs <= 9) hrs = "0" + hrs;
            if (min <= 9) min = "0" + min;
            if (sec <= 9) sec = "0" + sec;
            var newTime = (hrs + ":" + min + ":" + sec);

            return newTime;
        }

        // Дістаєм строку з текстового поля start1.
        var t1 = document.getElementById("start-time" + i).value;
        // переводимо в дату
        let time1 = strToTime(t1);

        // Дістаєм строку з текстового поля duration1.
        var t2 = document.getElementById("duration" + i).value;
        // Переводимо в дату
        let time2 = strToTime(t2);

        // Функція додає два проміжки часу і виводить результат.
        function timeSum(time1, time2) {

            // Розбиваєм строку часу на масив.
            var ex1 = time1.split(':');
            // Переводимо елементи масиву в integer.
            var h1 = Number.parseInt(ex1[0]);
            var m1 = Number.parseInt(ex1[1]);
            var s1 = Number.parseInt(ex1[2]);
            // Розбиваєм строку часу на масив.
            var ex2 = time2.split(':');
            // Переводимо елементи масиву в integer.
            var h2 = Number.parseInt(ex2[0]);
            var m2 = Number.parseInt(ex2[1]);
            var s2 = Number.parseInt(ex2[2]);

            // var s = s1 + s2;
            var seconds = '00';
            var minutes = m1 + m2;
            var m = (m1 + m2);
            if (m >= 60) {
                minutes = m - 60;
                if (m == 60) {
                    minutes = '00';
                }
                h1 = h1 + 1;
            }
            var hours = h1 + h2;
            if (hours > 24) {
                hours = '00';
            }
            if (minutes < 10) minutes = "0" + minutes;
            if (hours < 10) hours = "0" + hours;

            // Повертаємо час в форматі "00:00:00".
            return hours + ':' + minutes + ':' + seconds;
        }

        // Додаємо два проміжки часу.
        var timeSumma = timeSum(time1, time2);

        document.getElementById("finish-time" + i).value = timeSumma;

        // Міняєм початок та тривалість записів в одному дні, при зміні одного з них.
        var n = i + 1;
        var currentDateStart = document.getElementById("start-date" + i).value;
        var nextDateStart = document.getElementById("start-date" + n).value;
        if (currentDateStart == nextDateStart) {
            document.getElementById("start-time" + n).value = document.getElementById("finish-time" + i).value;
        } else {
            document.getElementById("start-time" + n).value = '00:01:00';
        }
    }
}
